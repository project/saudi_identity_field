<?php

namespace Drupal\saudi_identity_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'saudi_identity' field type.
 *
 * @FieldType(
 *   id = "saudi_identity",
 *   label = @Translation("Saudi Identity"),
 *   description = @Translation("Containing Saudi identity field value."),
 *   default_widget = "saudi_identity_default",
 *   default_formatter = "basic_string"
 * )
 * @category Saudi_Identity
 * @package Saudi_Identity
 * @license https://www.drupal.org/project/saudi_identity_field saudi_identity
 * @version Release: 8.2.1
 * @link https://www.drupal.org/project/saudi_identity_field
 */
class SaudiIdentityItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'id_types' => [
        'saudi' => t('Saudi Identity'),
        'muqeem' => t('Muqeem Identity'),
        'gcc' => t('GCC'),
      ],
      'unique' => 0,
    ] + parent::defaultFieldSettings();
  }

  /**
   * Returns a form for the field-level settings.
   *
   * Invoked from \Drupal\field_ui\Form\FieldConfigEditForm to allow
   * administrators to configure field-level settings.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   *
   * @return array
   *   The form definition for the field settings.
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    return [
      'id_types' => [
        '#type' => 'checkboxes',
        '#title' => $this->t('Accepted ID types'),
        '#default_value' => $this->getSetting('id_types'),
        '#options' => self::defaultFieldSettings()['id_types'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 64,
        ],
        'type' => [
          'type' => 'varchar',
          'length' => 64,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Saudi Identity'))
      ->setRequired(TRUE);

    $properties['type'] = DataDefinition::create('string')
      ->setLabel(t('Saudi Identity Type'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = 1234567890;
    $values['type'] = 'gcc';
    return $values;
  }

}
