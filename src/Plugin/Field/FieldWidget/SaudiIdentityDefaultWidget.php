<?php

namespace Drupal\saudi_identity_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use top7up\validateSAID;

/**
 * Plugin implementation of the 'saudi_identity_default' widget.
 *
 * @FieldWidget(
 *   id = "saudi_identity_default",
 *   label = @Translation("Saudi Identity"),
 *   field_types = {
 *     "saudi_identity"
 *   }
 * )
 */
class SaudiIdentityDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(
        FieldItemListInterface $items,
        $delta,
        array $element,
        array &$form,
        FormStateInterface $form_state
    ) {
    // @todo We have to load these options from this field settings to show
    // only selected types.
    $saudi_identity_options = [
      'saudi' => $this->t('Saudi Identity'),
      'muqeem' => $this->t('Muqeem  Identity'),
      'gcc' => $this->t('GCC'),
    ];

    $allowed_id_types = [];
    if ($items->getSetting('id_types')) {
      // @todo Please review this part.
      foreach ($items->getSetting('id_types') as $id_type => $value) {
        if (($id_type == $value) && ($value)) {
          $allowed_id_types[$value] = $saudi_identity_options[$value];
        }
      }
    }

    $element['saudi_identity_container'] = [
      '#type' => 'fieldset',
      '#attributes' => ['class' => ['container-inline']],
      '#title' => $this->t('Saudi ID'),
    ];
    $element['saudi_identity_container']['type'] = [
      '#type' => 'radios',
      '#required' => $element['#required'],
      '#title' => $this->t('ID type'),
      '#options' => $allowed_id_types,
      '#default_value' => isset($items[$delta]->type) ? $items[$delta]->type : NULL,
      // Here no need to have an element_validate.
    ];
    $element['saudi_identity_container']['value'] = [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
    ];

    $element['#element_validate'][] = [static::class, 'validateSaudiIdentity'];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value['type'] = $value['saudi_identity_container']['type'];
      $value['value'] = $value['saudi_identity_container']['value'];
      unset($value['saudi_identity_container']);
    }

    return $values;
  }

  /**
   * Validate field.
   *
   * @param array $element
   *   Validation array for the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state variables.
   */
  public static function validateSaudiIdentity(array $element, FormStateInterface $form_state) {
    $field = $element['saudi_identity_container'];
    $id_type = $field['type']['#value'];
    $id_number = $field['value']['#value'];
    $validation = FALSE;
    switch ($id_type) {
      case 'muqeem':
        // Making sure the muqeem ID number is 10 digits and starting with 2.
        if (2 == substr($id_number, 0, 1) && (strlen($id_number) == 10)) {
          $validation = TRUE;
        }
        break;

      case 'gcc':
        // Currently there are no validation for this field type.
        if (is_numeric($id_number) && (strlen($id_number) > 0)) {
          $validation = TRUE;
        }
        break;

      case 'saudi':
        // Check Saudi ID number using the needful algorithm.
        if ((new validateSAID)->check($id_number) == 1) {
          $validation = TRUE;
        }
        break;

      // For default we will do nothing.
      default:
        break;
    }

    // If validation failed we should show error.
    if (!$validation) {
      // @todo Enhance the error message.
      if ($element['#required'] || (strlen($id_number) > 0) || (strlen($id_type) > 0)) {
        $form_state->setErrorByName(
              implode('][', $element['#parents']),
              $this->t('Please provide a valid ID number.')
          );
      }
    }
  }

}
